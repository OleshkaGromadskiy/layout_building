import 'package:flutter/material.dart';
import 'package:layout_building_practice_1/my_app_bar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;

    return Scaffold(extendBodyBehindAppBar: true,
      appBar: MyAppBar(),
      body: Container(
        color: Color.fromARGB(255, 244, 234, 221),
        child: Stack(
          children: [
            Positioned(
              right: 0,
              top: _height / 10,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.transparent,
                  border: Border.all(
                    color: Color.fromARGB(255, 233, 93, 85),
                    width: 2,
                  ),
                ),
                height: _height / 3,
                width: _height / 3,
              ),
            ),
            Positioned(
              right: -_height / 4,
              top: -_height / 8,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromARGB(255, 233, 93, 85),
                ),
                height: _height / 2,
                width: _height / 2,
              ),
            ),
            Positioned(
              right: -_height / 12,
              top: _height / 6,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.transparent,
                  border: Border.all(
                    color: Colors.white,
                    width: 2,
                  ),
                ),
                height: _height / 6,
                width: _height / 6,
              ),
            ),
            Positioned(
              bottom: -_height / 8,
              left: _height / 40,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.transparent,
                  border: Border.all(
                    color: Color.fromARGB(255, 233, 93, 85),
                    width: 2,
                  ),
                ),
                height: _height / 5,
                width: _height / 5,
              ),
            ),
            Positioned(
              bottom: -_height / 10,
              left: -_height / 10,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromARGB(255, 233, 93, 85),
                ),
                height: _height / 4,
                width: _height / 4,
              ),
            ),
            Positioned(
              bottom: _height / 10,
              left: -_height / 20,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.transparent,
                  border: Border.all(
                    color: Colors.white,
                    width: 2,
                  ),
                ),
                height: _height / 10,
                width: _height / 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
