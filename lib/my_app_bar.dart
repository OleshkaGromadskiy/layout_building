import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(100);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(MediaQuery.of(context).padding.top),
      child: Container(
        padding: EdgeInsets.only(bottom: 25, top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Custom AppBar"),
            Icon(
              Icons.menu,
              color: Color.fromARGB(255, 74, 74, 74),
            ),
          ],
        ),
        decoration: BoxDecoration(
          border: Border.symmetric(
            horizontal: BorderSide(
              color: Color.fromARGB(255, 74, 74, 74),
              width: 2,
            ),
          ),
        ),
      ),
    );
  }
}
